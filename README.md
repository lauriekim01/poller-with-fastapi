# Poller with FastAPI

This project is an example of how to use a background **poller** process to keep
value objects in one service synced up with their source entities in another
service.

## Dev setup

```bash
docker volume create poller-data
docker-compose build
docker-compose up
```

## Deployment todos:

(pre-work) : read ["Getting Started"](https://render.com/docs/infrastructure-as-code) and then
the `render.yaml` in this repo and see if you can figure out what it does.

### Just 5 **short** steps 😸

* On render.com create a new blueprint
* Point it at your GitLab project
* Enter a Service Group Name and click create
* In the sales service, the initial value for `INVENTORY_API_HOST` won't quite
  be correct. Get the value for your inventory service and update the
  environment variable in the sales service. It will be something like
  `https://inventory-api-7ru4.onrender.com` (no trailing `/`)
* The sales-api service will likely fail on the initial deploy. Manually
  deploy the service a time or two and it will come up :-)

## Project overview

The application is composed of 2 services plus a database.

The **Inventory** service is the owning service of the Automobile entity and it
has an API endpoint for getting all of the autos in the inventory.

The **Sales** service needs to reference the Automobiles from the **Inventory**
and it uses AutoVO value objects to refer to them.

The **Sales Poller** lives inside of the sales service and runs in the same
FastAPI instance. Look inside of `main.py` and `sales/poller.py` for more
details.🔍
